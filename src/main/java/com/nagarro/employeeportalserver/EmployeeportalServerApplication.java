package com.nagarro.employeeportalserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@ComponentScan({ "com.nagarro" })
public class EmployeeportalServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeportalServerApplication.class, args);
	}

}
