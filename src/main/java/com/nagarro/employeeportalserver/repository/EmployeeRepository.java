package com.nagarro.employeeportalserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nagarro.employeeportalserver.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
