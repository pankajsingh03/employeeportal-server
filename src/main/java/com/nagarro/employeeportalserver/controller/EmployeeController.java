package com.nagarro.employeeportalserver.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.employeeportalserver.model.Employee;
import com.nagarro.employeeportalserver.service.EmployeeService;

@RestController
public class EmployeeController {
	private EmployeeService employeeService;

	public EmployeeController(EmployeeService employeeService) {
		super();
		this.employeeService = employeeService;
	}

	@RequestMapping("/")
	public String home() {
		System.out.println("inside the home function");
		return "home.jsp";
	}

	// build create employee by rest api
	@PostMapping("/api/employees")
	public ResponseEntity<Employee> saveEmployee(@RequestBody Employee employee) {
		return new ResponseEntity<Employee>(employeeService.saveEmployee(employee), HttpStatus.CREATED);
	}

	// build get all employee by REST API
	@GetMapping("/api/employees")
	public List<Employee> getAllEmployee() {
		return employeeService.getAllEmployees();
	}

	// build get employee by id by REST API
	@GetMapping("/api/employees/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") int id) {
		return new ResponseEntity<Employee>(employeeService.getEmployeeById(id), HttpStatus.OK);
	}

	// build update Employee by REST API
	@PutMapping("/api/employees/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee) {
		return new ResponseEntity<Employee>(employeeService.updateEmployee(employee, id), HttpStatus.OK);
	}

	// build delete Employee by REST API
	@DeleteMapping("/api/employees/{id}")
	public ResponseEntity<String> deleteEmployee(@PathVariable("id") int id) {
		// delete employee from db
		employeeService.deleteEmployee(id);

		return new ResponseEntity<String>("employee deleted successfully!", HttpStatus.OK);
	}
}
