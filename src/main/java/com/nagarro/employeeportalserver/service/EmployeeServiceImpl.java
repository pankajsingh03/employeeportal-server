package com.nagarro.employeeportalserver.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.employeeportalserver.model.Employee;
import com.nagarro.employeeportalserver.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeRepository employeeRepository;

	public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
		super();
		this.employeeRepository = employeeRepository;
	}

	@Override
	public Employee saveEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	@Override
	public List<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public Employee getEmployeeById(int id) {
		Optional<Employee> employee = employeeRepository.findById(id);
		return employeeRepository.findById(id).orElse(null);

	}

	@Override
	public Employee updateEmployee(Employee employee, int id) {

		// we need to check wheather a employee with given id is avilable or naot
		Employee e = employeeRepository.findById(id).orElse(null);
		if (e == null) {
			employeeRepository.save(employee);

			return employee;
		}
		e.setEmployeeName(employee.getEmployeeName());
		e.setLocation(employee.getLocation());
		e.setEmail(employee.getEmail());
		e.setDateOfBirth(employee.getDateOfBirth());

		employeeRepository.save(e);

		return e;
	}

	@Override
	public void deleteEmployee(int id) {
		// check if exist
		Employee employee = employeeRepository.findById(id).orElse(null);
		if (employee != null) {
			employeeRepository.deleteById(id);
		}

	}

}
