package com.nagarro.employeeportalserver.service;

import java.util.List;

import com.nagarro.employeeportalserver.model.Employee;

public interface EmployeeService {

	Employee saveEmployee(Employee employee);
	List<Employee> getAllEmployees();
	Employee updateEmployee(Employee employee, int id);
	Employee getEmployeeById(int id);
	void deleteEmployee(int id);
}
